package com.alfatecsistemas.sihna.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.alfatecsistemas.sihna.entity.dto.Paciente;

@RestController
@RequestMapping("/angularjsTest")
public class AngularJsPruebaController {

	final List<Paciente> listaPaciente;

	public AngularJsPruebaController() {
		listaPaciente = new ArrayList<>();
		listaPaciente.add(new Paciente(1L, "Cesar", "Fajardo", "Ramos", "M", Calendar.getInstance().getTime(), "123"));
		listaPaciente
				.add(new Paciente(2L, "Angela", "Barragan", "Camacho", "F", Calendar.getInstance().getTime(), "124"));
	}

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<Paciente>> load() {
		return new ResponseEntity<List<Paciente>>(listaPaciente, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<Paciente> get(@PathVariable("id") Long id) {
		Optional<Paciente> itemPaciente = listaPaciente.stream().filter(item -> item.getNhc().equals(id)).findFirst();
		if (itemPaciente.isPresent()) {
			return new ResponseEntity<Paciente>(itemPaciente.get(), HttpStatus.OK);
		}
		return new ResponseEntity<Paciente>(HttpStatus.NOT_FOUND);
	}

	// =========================================== Delete Paciente
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		Optional<Paciente> itemPaciente = listaPaciente.stream().filter(item -> item.getNhc().equals(id)).findFirst();
		if (!itemPaciente.isPresent()) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		if (listaPaciente.remove(itemPaciente.get())) {
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.EXPECTATION_FAILED);
	}

	// =========================================== Create Paciente

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody Paciente paciente, UriComponentsBuilder ucBuilder) {
		Optional<Paciente> itemPaciente = listaPaciente.stream().filter(item -> item.getNhc().equals(paciente.getNhc()))
				.findFirst();
		if (itemPaciente.isPresent()) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		listaPaciente.add(paciente);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/angularjsTest/{id}").buildAndExpand(paciente.getNhc()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	// =========================================== Update Paciente

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Paciente> update(@RequestBody Paciente paciente) {
		Optional<Paciente> itemPaciente = listaPaciente.stream().filter(item -> item.getNhc().equals(paciente.getNhc()))
				.findFirst();
		if (!itemPaciente.isPresent()) {
			return new ResponseEntity<Paciente>(HttpStatus.NOT_FOUND);
		}
		listaPaciente.remove(itemPaciente.get());
		listaPaciente.add(paciente);

		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
	}
}
