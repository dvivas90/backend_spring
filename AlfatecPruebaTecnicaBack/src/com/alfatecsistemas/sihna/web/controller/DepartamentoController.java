package com.alfatecsistemas.sihna.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alfatecsistemas.sihna.entity.Departamento;
import com.alfatecsistemas.sihna.facade.DepartametoServiceFacade;

@RestController
@RequestMapping("/serviceDepartamento")
public class DepartamentoController {

	@Autowired
	private DepartametoServiceFacade departameService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> crear(@RequestBody Departamento departamento) {
		try {
			departameService.crear(departamento);
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		try {
			if (departameService.eliminar(id)) {
				return new ResponseEntity<Void>(HttpStatus.OK);
			}
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
