package com.alfatecsistemas.sihna.web.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alfatecsistemas.sihna.entity.Empleado;
import com.alfatecsistemas.sihna.entity.dto.EmpleadoDto;
import com.alfatecsistemas.sihna.entity.dto.Paciente;
import com.alfatecsistemas.sihna.facade.EmpleadoServiceFacade;

@RestController
@RequestMapping("/serviceEmpleado")
public class EmpleadoController {

	@Autowired
	private EmpleadoServiceFacade empleadoService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> crear(@RequestBody EmpleadoDto empleadoDto) throws Exception {
		try {
			empleadoService.crear(empleadoDto);
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		try {
			if (empleadoService.eliminar(id)) {
				return new ResponseEntity<Void>(HttpStatus.OK);
			}
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/consultarPorDepartamento/{id}", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<List<EmpleadoDto>> findbyDepartamento(
			@PathVariable(value = "idDepartamento") Long idDepartamento) throws Exception {
		return new ResponseEntity<List<EmpleadoDto>>(empleadoService.consultarPorDepartamento(idDepartamento),
				HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<EmpleadoDto> findbyEmpleado(@PathVariable("idEmpleado") Long idEmpleado) throws Exception {
		Optional<EmpleadoDto> itemEmpleado = empleadoService.consultarPorIdEmpleado(idEmpleado);
		if (itemEmpleado.isPresent()) {
			return new ResponseEntity<EmpleadoDto>(itemEmpleado.get(), HttpStatus.OK);
		} 
		return new ResponseEntity<EmpleadoDto>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public ResponseEntity<EmpleadoDto> edit(@PathVariable("id") Long id) throws Exception {
		return empleadoService.editarPorId(empleado, StringUtils.isEmpty(nombre) ? null : nombre,
				StringUtils.isEmpty(apellido) ? null : apellido, departamento == 0 ? null : departamento);
	}
	
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Paciente> update(@RequestBody Paciente paciente) {
		Optional<Paciente> itemPaciente = listaPaciente.stream().filter(item -> item.getNhc().equals(paciente.getNhc()))
				.findFirst();
		if (!itemPaciente.isPresent()) {
			return new ResponseEntity<Paciente>(HttpStatus.NOT_FOUND);
		}
		listaPaciente.remove(itemPaciente.get());
		listaPaciente.add(paciente);

		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
	}
}
