package com.alfatecsistemas.sihna.facade;

import java.util.List;
import java.util.Optional;

import com.alfatecsistemas.sihna.entity.dto.EmpleadoDto;

public interface EmpleadoServiceFacade {

	void crear(EmpleadoDto entityDto) throws Exception;

	boolean eliminar(final Long id) throws Exception;

	List<EmpleadoDto> consultarPorDepartamento(final Long idDepartamento);

	Optional<EmpleadoDto> consultarPorIdEmpleado(final Long idEmpleado);

}
