package com.alfatecsistemas.sihna.facade;

import com.alfatecsistemas.sihna.entity.Departamento;

public interface DepartametoServiceFacade {

	void crear(Departamento departamento) throws Exception;

	boolean eliminar(Long id) throws Exception;

}
