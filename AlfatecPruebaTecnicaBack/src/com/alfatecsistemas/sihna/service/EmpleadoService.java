package com.alfatecsistemas.sihna.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alfatecsistemas.sihna.entity.Empleado;
import com.alfatecsistemas.sihna.entity.dto.EmpleadoDto;
import com.alfatecsistemas.sihna.facade.EmpleadoServiceFacade;

@Service
public class EmpleadoService implements EmpleadoServiceFacade {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void crear(final EmpleadoDto entityDto) {
		Empleado entity = new Empleado(entityDto.getName(), entityDto.getLastName(), entityDto.getIdDepartamento());
		em.persist(entity);
	}

	@Transactional
	public boolean eliminar(final Long id) {
		if (em.createNativeQuery("DELETE FROM Empleado WHERE id = ?1").setParameter(1, id).executeUpdate() > 0) {
			return true;
		}
		return false;
	}

	public List<EmpleadoDto> consultarPorDepartamento(final Long idDepartamento) {
		final List<EmpleadoDto> listEmpleadoDto = new ArrayList<>();
		final List<Empleado> listItem = em
				.createQuery("SELECT emp FROM Empleado emp JOIN FETCH emp.departamento dep WHERE dep.id = ?1",
						Empleado.class)
				.setParameter(1, idDepartamento).getResultList();
		for (Empleado empleado : listItem) {
			listEmpleadoDto.add(new EmpleadoDto(empleado.getId(), empleado.getName(), empleado.getLastName(),
					empleado.getIdDepartamento()));
		}
		return listEmpleadoDto;
	}

	public Optional<EmpleadoDto> consultarPorIdEmpleado(final Long idEmpleado) {
		try {
			final Empleado entity = em
					.createQuery("SELECT emp FROM Empleado emp JOIN FETCH emp.departamento dep WHERE emp.id = ?1",
							Empleado.class)
					.setParameter(1, idEmpleado).getSingleResult();
			return Optional.of(new EmpleadoDto(entity.getId(), entity.getName(), entity.getLastName(),
					entity.getIdDepartamento()));
		} catch (NoResultException e) {
			return Optional.empty();
		}
	}

	@Transactional
	public Empleado editarPorId(final Long idEmpleado, final String name, final String lastName,
			final Long idDepartamento) {
		Optional<Empleado> optEntity = consultarPorIdEmpleado(idEmpleado);
		if (name != null) {
			optEntity.get().setName(name);
		}
		if (lastName != null) {
			optEntity.get().setLastName(lastName);
		}
		if (idDepartamento != null) {
			optEntity.get().setIdDepartamento(idDepartamento);
		}
		em.merge(optEntity.get());
		return optEntity.get();
	}
}
