package com.alfatecsistemas.sihna.service;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alfatecsistemas.sihna.entity.Departamento;
import com.alfatecsistemas.sihna.facade.DepartametoServiceFacade;

@Service
public class DepartametoService implements DepartametoServiceFacade {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void crear(final Departamento entity) throws Exception {
		em.persist(entity);
	}

	public Optional<Departamento> consultarPorId(final Long id) {
		try {
			return Optional
					.of(em.createNamedQuery("SELECT depa FROM Departamento depa WHERE depa.id = ?1", Departamento.class)
							.setParameter(1, id).getSingleResult());
		} catch (NoResultException e) {
			return Optional.empty();
		}
	}

	@Transactional
	public boolean eliminar(final Long id) {
		final Optional<Departamento> optDepartamento = consultarPorId(id);
		if (optDepartamento.isPresent()) {
			em.remove(optDepartamento.get());
			return true;
		}
		return false;
	}

}
