package com.alfatecsistemas.sihna.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table
@Entity
public class Empleado implements Serializable {

	private static final long serialVersionUID = 7383649213613589400L;
	private Long id;
	private String name;
	private String lastName;
	private Departamento departamento;
	private Long idDepartamento;

	public Empleado() {
	}

	public Empleado(String name, String lastName, Long idDepartamento) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.idDepartamento = idDepartamento;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IdDepartamento", insertable = false, updatable = false)
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Long getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
}
