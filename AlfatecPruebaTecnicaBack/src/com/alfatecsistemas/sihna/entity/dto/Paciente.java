package com.alfatecsistemas.sihna.entity.dto;

import java.util.Date;

public class Paciente {

	private Long nhc;
	private String nombre;
	private String primerApellido;
	private String segundoApellido;
	private String genero;
	private Date fechaNacimiento;
	private String nif;

	public Paciente() {
	}

	public Paciente(Long nhc, String nombre, String primerApellido, String segundoApellido, String genero,
			Date fechaNacimiento, String nif) {
		super();
		this.nhc = nhc;
		this.nombre = nombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.genero = genero;
		this.fechaNacimiento = fechaNacimiento;
		this.nif = nif;
	}

	public Long getNhc() {
		return nhc;
	}

	public void setNhc(Long nhc) {
		this.nhc = nhc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}
}
