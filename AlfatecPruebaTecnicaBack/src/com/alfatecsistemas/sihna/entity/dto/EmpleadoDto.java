package com.alfatecsistemas.sihna.entity.dto;

import java.io.Serializable;

public class EmpleadoDto implements Serializable {
	private static final long serialVersionUID = -3258918536045583644L;
	private Long id;
	private String name;
	private String lastName;
	private Long idDepartamento;

	public EmpleadoDto() {
	}

	public EmpleadoDto(Long id, String name, String lastName, Long idDepartamento) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.idDepartamento = idDepartamento;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
}
