var module = angular.module('pacienteApp.services', []);

// Factory, crud paciente
module
		.factory(
				'Paciente',
				function($http) {
					return {
						list : function() {
							console.log('Ingreso List ');
							var response = $http
									.get('http://localhost:8080/AlfatecPruebaTecnicaBack/angularjsTest');
							return response;
						},
						get : function(value){
							console.log('Ingreso Get '+value);
							var response = $http
									.get('http://localhost:8080/AlfatecPruebaTecnicaBack/angularjsTest/'+value);
							return response;
						},
						eliminar : function(value) {
							console.log('Ingreso eliminar '+value);
							var response = $http
									.delete('http://localhost:8080/AlfatecPruebaTecnicaBack/angularjsTest/'+value);
							return response;
						},
						guardar : function(paciente){
							console.log('Ingreso guardar ');
							console.log(paciente);
							var response = $http
								.post('http://localhost:8080/AlfatecPruebaTecnicaBack/angularjsTest', paciente);
							return response;
						},
						actualizar : function(paciente){
							console.log('Ingreso Actualizar ');
							console.log(paciente);
							var response = $http
								.put('http://localhost:8080/AlfatecPruebaTecnicaBack/angularjsTest', paciente);
							return response;
						}
					}
				});
// Services
module.service('popupService', function($window) {
	this.showPopup = function(message) {
		return $window.confirm(message);
	}
});