var module = angular.module('pacienteApp.controllers', []);

// Controller Listar Pacientes
module.controller('PacienteListController', function($scope, $state,
		popupService, $window, Paciente) {
	$scope.deleteMovie = function(item) {
		if (popupService.showPopup("Desea eliminar al paciente " + item.nombre
				+ " " + item.primerApellido + "?")) {
			Paciente.eliminar(item.nhc).success(function() {
				$window.location.href = '';
			});
		}
	}

	$scope.load = function() {
		Paciente.list().then(function(result) {
			$scope.pacientes = result.data;
		});
	}
	$scope.load();
});

// Controller Ver paciente
module.controller('PacienteViewController', function($scope, $stateParams,
		Paciente) {
	console.log('View');
	Paciente.get($stateParams.id).then(function(result) {
		$scope.paciente = result.data;
	});
});

// Controller Crear Paciente
module
		.controller(
				'PacienteCreateController',
				function($scope, $state, $stateParams, Paciente, popupService) {

					$scope.paciente = {};

					$scope.addPaciente = function() {
						Paciente
								.guardar($scope.paciente)
								.success(function() {
									$state.go('pacientes');
								})
								.error(
										function(msg) {
											popupService
													.showPopup('Se ha generado un error al momento de guardar.');
										});
					}
				})

// Controller Editar paciente
module
		.controller(
				'PacienteEditController',
				function($scope, $state, $stateParams, Paciente) {
					Paciente.get($stateParams.id).then(function(result) {
						$scope.paciente = result.data;
					});
					$scope.update= true;
					$scope.updatePaciente = function() {
						Paciente
								.actualizar($scope.paciente)
								.success(function() {
									$state.go('pacientes');
								})
								.error(
										function(msg) {
											popupService
													.showPopup('Se ha generado un error al momento de Editar.');
										});
					};
				});